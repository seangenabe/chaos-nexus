import { AiSubject } from "ai-subject"
import { Channel, connect, ConsumeMessage, Options } from "amqplib"
import "fastify"
import fp from "fastify-plugin"

export default fp(
  async function rabbitmqPlugin(
    server,
    {
      rabbitMq: { connection: connectionString, exchangeName = "pipelines" }
    }: RabbitMqPluginOptions
  ) {
    const connection = await connect(connectionString)
    const channel = await connection.createChannel()

    channel.assertExchange(exchangeName, "topic", { durable: true })

    server.decorate("rabbitmq", {
      channel,
      exchangeName,
      publish(routingKey: string, content: Buffer, options?: Options.Publish) {
        channel.publish(exchangeName, routingKey, content, options)
      },
      async *subscribe(
        pattern: string,
        {
          serverArgs,
          consumeOptions
        }: { serverArgs?: any; consumeOptions?: Options.Consume } = {}
      ) {
        const q = await channel.assertQueue("", { exclusive: true })
        const subject = new AiSubject<ConsumeMessage>()
        channel.bindQueue(q.queue, exchangeName, pattern, serverArgs)
        channel.consume(
          q.queue,
          msg => {
            if (msg == null) {
              subject.complete()
              return
            }
            subject.next(msg)
          },
          consumeOptions
        )
        yield* subject
      }
    })
  },
  {
    name: "@chaos-nexus/decorator-rabbitmq",
    fastify: "^2.0.0"
  }
)

export interface RabbitMqPluginOptions {
  rabbitMq: {
    connection: string | Options.Connect
    exchangeName?: string
  }
}

export interface RabbitMqDecorator {
  channel: Channel
  exchangeName: string
  publish(
    routingKey: string,
    content: Buffer,
    options?: Options.Publish
  ): boolean
  subscribe(
    pattern: string,
    options?: { serverArgs?: any; consumeOptions?: Options.Consume }
  ): AsyncIterableIterator<ConsumeMessage>
}

declare module "fastify" {
  interface FastifyInstance<HttpServer, HttpRequest, HttpResponse> {
    rabbitmq: RabbitMqDecorator
  }
}
