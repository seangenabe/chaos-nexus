import { ok } from "assert"
import fastify from "fastify"
import { run, test } from "t0"
import decMinio from "."

test("register", async () => {
  const server = fastify()
  server.register(decMinio, {
    minioOptions: {
      endPoint: "localhost",
      accessKey: "minioadmin",
      secretKey: "minioadmin",
      useSSL: false,
      port: 9000
    }
  })
  await server.ready()
  ok(server.hasDecorator("minio"))
})

run()
