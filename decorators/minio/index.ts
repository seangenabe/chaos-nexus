import "fastify"
import fp from "fastify-plugin"
import { Client, ClientOptions } from "minio"

export default fp<unknown, unknown, unknown, MinioPluginOptions>(
  function minioPlugin(
    server,
    { bucketName = "chaos-nexus", minioOptions }: MinioPluginOptions,
    next
  ) {
    const client = new Client(minioOptions)
    server.decorate("minio", { client, bucketName })

    if (!client.bucketExists(bucketName)) {
      client.makeBucket(bucketName, "us-east-1")
    }

    next()
  },
  {
    name: "@chaos-nexus/decorator-minio",
    fastify: "^2.0.0"
  }
)

export interface MinioPluginOptions {
  minioOptions: ClientOptions
  bucketName?: string
}

export interface MinioDecorator {
  readonly client: Client
  readonly bucketName: string
}

declare module "minio" {
  interface BucketStream<T> {
    [Symbol.asyncIterator](): AsyncIterableIterator<T>
  }
}

declare module "fastify" {
  interface FastifyInstance<HttpServer, HttpRequest, HttpResponse> {
    readonly minio: MinioDecorator
  }
}
