import { AiSubject } from "ai-subject"
import { FastifyRequest } from "fastify"
import "fastify-multipart"
import fp from "fastify-plugin"
import { Readable } from "stream"

export default fp<unknown, unknown, unknown, {}>(
  async function multipartAiPlugin(server) {
    server.decorateRequest("multipartAi", function(this: FastifyRequest) {
      const subject = new AiSubject<FieldEntry>()
      this.multipart(
        (field, file: Readable, filename, encoding, mimetype) => {
          subject.next({ field, file, filename, encoding, mimetype })
        },
        (err?: Error) => (err == null ? subject.error(err) : subject.complete())
      )
      return subject[Symbol.asyncIterator]()
    })
  },
  {
    name: "@chaos-nexus/decorator-multipart-ai",
    fastify: "^2.0.0"
  }
)

export interface FieldEntry {
  field: string
  file: Readable
  filename: string
  encoding: string
  mimetype: string
}

declare module "fastify" {
  interface FastifyRequest {
    multipartAi(): AsyncIterableIterator<FieldEntry>
  }
}
