import { Collection } from "mongodb"
import { getParentSpec } from "./get-parent-spec"
import { hashSpec } from "./hash-spec"
import { PackInfo, PackSpec } from "./types"

/**
 * Generates pack metadata for a given spec.
 * @param packsCollection
 * @param spec
 */
export async function generatePackMetadata(
  packsCollection: Collection<PackInfo>,
  spec: PackSpec
): Promise<PackInfo> {
  const hash = hashSpec(spec)
  const meta = await packsCollection.findOne({ _id: hash })
  if (meta == null) {
    const parentSpec = getParentSpec(spec)
    const parentHash =
      parentSpec == null
        ? undefined
        : (await generatePackMetadata(packsCollection, parentSpec))._id

    const info: PackInfo = {
      _id: hash,
      exists: false,
      parent: parentHash
    }
    await packsCollection.insertOne(info)
    return info
  }

  return meta
}
