import { Readable } from "stream"
import { Promisable } from "type-fest"

/**
 * Represents a collection of files in some hierarchy.
 *
 * Packs are immutable and can only be modified by transforming it into
 * another pack.
 *
 * Note: Directories cannot be saved in a pack; however,
 * files can have / paths a la S3/Minio.
 *
 */
export interface Pack {
  readonly files: AsyncIterable<PackFile>
  meta?: unknown
}

/**
 * Represents a file that can be read.
 *
 * Pack files are immutable and can only be modified by transforming it
 * into another file and saving the result into another pack.
 */
export interface PackFile {
  /**
   * The path of the file in the hierarchy.
   */
  readonly path: string
  /**
   * When implemented, this method should return a new readable stream
   * containing the contents of the file.
   */
  getStream(): Readable
}

/**
 * Defines a pipeline that contains all of the data required to create
 * or recreate a pack.
 *
 * Packs are indexed by the hash of its defining spec.
 */
export interface PackSpec {
  source: SourceSpec
  transforms: readonly TransformSpec[]
}

/**
 * When implemented, contains the data needed to create a pack from
 * information from an external source.
 */
export interface SourceSpec {
  /**
   * Disambiguates the sourcer to use for creating the pack.
   */
  type: string
}

/**
 * When implemented, contains the data needed to transform a pack
 * into another pack.
 */
export interface TransformSpec {
  /**
   * Disambiguates the transform to use for transforming a pack.
   */
  type: string
}

/**
 * A function that creates a pack from an external source.
 */
export interface Sourcer {
  (spec: SourceSpec): Promisable<Pack>
}

/**
 * A function that transforms an existing pack into a new pack.
 */
export interface Transform {
  (spec: TransformSpec, input: Pack): Promisable<Pack>
}

/**
 * Pack info as saved in the database.
 */
export interface PackInfo<Meta = unknown> {
  _id: string // = hash
  exists: boolean
  expiry?: Date
  parent?: string
  meta?: Meta
}
