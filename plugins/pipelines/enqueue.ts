import { RabbitMqDecorator } from "@chaos-nexus/decorator-rabbitmq"
import { Collection } from "mongodb"
import { generatePackMetadata } from "./generate-pack-metadata"
import { getParentSpec } from "./get-parent-spec"
import { hashSpec } from "./hash-spec"
import { toBuffer } from "./to-buffer"
import { PackInfo, PackSpec } from "./types"

/**
 * Enqueue a spec for processing.
 */
export async function enqueue(
  packsCollection: Collection<PackInfo>,
  rabbitmq: RabbitMqDecorator,
  spec: PackSpec
): Promise<void> {
  const { publish, subscribe } = rabbitmq
  const hash = hashSpec(spec)
  const info =
    (await packsCollection.findOne({ _id: hash })) ??
    (await generatePackMetadata(packsCollection, spec))

  if (info.exists) {
    return
  }

  if (info.parent == null) {
    // Source. Emit request to source.
    publish(`source.${spec.source.type}`, toBuffer(spec), { persistent: true })
    return
  }

  const parentInfo = (await packsCollection.findOne({ _id: info.parent }))!
  if (!parentInfo.exists) {
    // Parent also doesn't exist.
    await enqueue(packsCollection, rabbitmq, getParentSpec(spec)!)
    // Wait for parent to complete.
    for await (const msg of subscribe("complete")) {
      const completedHash = msg.content.toString("utf8")
      if (completedHash !== hash) continue
    }
  }

  // This pack doesn't exist but the parent already exists.
  // Emit request to transform.
  publish(`transform.${spec.transforms.slice(0, -1)[0].type}`, toBuffer(spec), {
    persistent: true
  })
}

export interface EnqueueResult {
  id: string
}
