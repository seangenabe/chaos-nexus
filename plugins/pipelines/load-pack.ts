import { MinioDecorator } from "@chaos-nexus/decorator-minio"
import { from } from "ix/asynciterable/from"
import { map } from "ix/asynciterable/operators/map"
import { BucketItem } from "minio"
import { resolve } from "path"
import { Readable } from "stream"
import { fnToAi } from "./fn-to-ai"
import { hashSpec } from "./hash-spec"
import { promiseStreamToStream } from "./promise-stream-to-stream"
import { Pack, PackFile, PackSpec } from "./types"

export function loadPack(
  minio: MinioDecorator,
  spec: PackSpec,
  meta: unknown
): Pack {
  const hash = hashSpec(spec)
  const bucketItemsSource = fnToAi(() =>
    minio.client
      .listObjectsV2(minio.bucketName, hash, true)
      [Symbol.asyncIterator]()
  )

  const files = from(bucketItemsSource).pipe(
    map<BucketItem, PackFile>(bucketItem => ({
      path: resolve(minio.bucketName, bucketItem.name),
      getStream: () =>
        promiseStreamToStream(
          minio.client.getObject(minio.bucketName, bucketItem.name) as Promise<
            Readable
          >
        )
    }))
  )

  const pack: Pack = { files, meta }
  return pack
}
