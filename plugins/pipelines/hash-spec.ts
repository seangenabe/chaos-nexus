import { PackSpec } from "./types"
import sss from "safe-stable-stringify"
import { createHash } from "crypto"
import { encode } from "safe-base64"

/**
 * Returns a base64url-encoded hash of the spec.
 * @param spec
 */
export function hashSpec(spec: PackSpec) {
  return encode(
    createHash("sha3-512")
      .update(sss(spec))
      .digest()
  )
}
