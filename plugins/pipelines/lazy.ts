export class Lazy<T> {
  constructor(private valueFactory: () => T) {}
  get value(): T {
    return this.valueFactory()
  }

  static fromValue<T>(value: T): Lazy<T> {
    return new Lazy(() => value)
  }
}
