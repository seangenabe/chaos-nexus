import { PassThrough, Readable, TransformOptions } from "stream"

export function promiseStreamToStream(
  input: Promise<Readable>,
  opts?: TransformOptions
): Readable
export function promiseStreamToStream<T>(
  input: Promise<Readable & AsyncIterable<T>>,
  opts?: TransformOptions
): Readable & AsyncIterable<T>
export function promiseStreamToStream<T>(
  input: Promise<Readable & AsyncIterable<T>>,
  opts?: TransformOptions
): Readable & AsyncIterable<T> {
  const pt = new PassThrough(opts)
  ;(async () => {
    try {
      const inputStream = await input
      inputStream.pipe(pt)
      inputStream.on("error", err => pt.emit("error", err))
    } catch (err) {
      pt.emit("error", err)
    }
  })()
  return pt
}
