# Pipelines plugin for Chaos Nexus

Pipelines for file and media storage.

## API

**Packs** are an immutable hierarchy of files. Packs are defined by an overall **pack spec**, which consists of a **source spec**, and one or more **transform specs**.

An sourcer defined by an source spec sources files into a pack.

A transform defined by a transform spec takes a pack as input and outputs another pack. Remember that packs are immutable. For example, transforms may change only one file in the pack. It may also return a completely different set of files.
