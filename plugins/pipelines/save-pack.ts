import { MinioDecorator } from "@chaos-nexus/decorator-minio"
import { toArray } from "ix/asynciterable/toarray"
import { hashSpec } from "./hash-spec"
import { Pack, PackSpec } from "./types"

export async function savePack(
  minio: MinioDecorator,
  spec: PackSpec,
  pack: Pack,
  overwrite?: boolean
) {
  const hash = hashSpec(spec)
  const bucketItems = await toArray(
    minio.client.listObjectsV2(minio.bucketName, hash, true)
  )
  if (bucketItems.length !== 0) {
    if (overwrite) {
      await minio.client.removeObjects(
        minio.bucketName,
        bucketItems.map(bucketItem => bucketItem.name)
      )
    } else {
      throw new Error("Pack already exists.")
    }
  }

  for await (const file of pack.files) {
    minio.client.putObject(
      minio.bucketName,
      `${hash}/${file.path}`,
      file.getStream()
    )
  }
}
