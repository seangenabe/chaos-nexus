import test from "ava"
import { hashSpec } from "../hash-spec"
import { server } from "./test-server"

test("source a pack", async t => {
  t.timeout(15000)
  await server.ready()
  t.true(server.hasDecorator("pipelines"))

  const spec = {
    source: { type: "saveText", value: "dog" },
    transforms: []
  }
  const hash = hashSpec(spec)
  const enqueueResult = await server.inject({
    url: "/enqueue",
    method: "POST",
    payload: spec
  })
  t.is(enqueueResult.statusCode, 200, enqueueResult.payload)
  t.is((JSON.parse(enqueueResult.payload) as { hash: string }).hash, hash)

  const getResult = await server.inject({ url: `/get/${hash}/value.txt` })
  t.is(getResult.payload, "dog")
})

test("source the same pack", async t => {
  const spec = {
    source: { type: "saveText", value: "cat" },
    transforms: []
  }
  const hash = hashSpec(spec)
  await server.inject({ url: "/enqueue", method: "POST", payload: spec })

  await server.inject({ url: `/get/${hash}/value.txt` })
  const getResult2 = await server.inject({ url: `/get/${hash}/value.txt` })
  t.is(getResult2.payload, "cat")
})
