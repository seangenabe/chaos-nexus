import decMinio from "@chaos-nexus/decorator-minio"
import decRabbitmq from "@chaos-nexus/decorator-rabbitmq"
import fastify from "fastify"
import FastifyMongodb from "fastify-mongodb"
import S from "fluent-schema"
import intoStream from "into-stream"
import { from } from "ix/asynciterable/from"
import { map } from "ix/asynciterable/operators/map"
import { Readable } from "stream"
import pipelines from ".."
import { Pack, PackFile, SourceSpec, TransformSpec } from "../types"

const server = fastify()
server.register(decMinio, {
  minioOptions: {
    port: 9000,
    endPoint: "localhost",
    useSSL: false,
    accessKey: "minioadmin",
    secretKey: "minioadmin"
  }
})
server.register(decRabbitmq, { rabbitMq: { connection: "amqp://localhost" } })
server.register(FastifyMongodb, { url: "mongodb://localhost/test" })
server.register(pipelines, {
  workerMode: { sourcersFilter: ["saveNumber"] }
})
server.after(err => {
  if (err) throw err
  server.pipelines.registerSourcer({
    type: "saveText",
    fn(spec: SaveTextSpec) {
      const pack: Pack = {
        files: from([
          {
            path: "value.txt",
            getStream() {
              return intoStream(`${spec.value}`)
            }
          }
        ])
      }
      return pack
    },
    schema: S.object()
      .prop("type", S.const("saveText"))
      .prop("value", S.string())
      .required(["type", "saveText"])
  })

  server.pipelines.registerTransform({
    type: "appendText",
    fn(spec: AppendTextSpec, input: Pack) {
      const output: Pack = {
        files: from(input.files).pipe(
          map((file: PackFile) => {
            if (file.path !== "value.txt") {
              return file
            }
            return {
              path: file.path,
              getStream: () => {
                return Readable.from(
                  (async function* appendText() {
                    yield* file.getStream()
                    yield spec.value
                  })()
                )
              }
            }
          })
        )
      }
      return output
    },
    schema: S.object
  })
})

export { server }

export interface SaveTextSpec extends SourceSpec {
  type: "saveText"
  value: string
}

export interface AppendTextSpec extends TransformSpec {
  type: "appendText"
  value: string
}
