export function toBuffer(obj: any): Buffer {
  return Buffer.from(JSON.stringify(obj))
}
