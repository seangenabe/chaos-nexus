import "@chaos-nexus/decorator-minio"
import "@chaos-nexus/decorator-rabbitmq"
import { ConsumeMessage } from "amqplib"
import { strict as strictAssert } from "assert"
import "fastify-mongodb"
import fp from "fastify-plugin"
import HttpErrors from "http-errors"
import { enqueue } from "./enqueue"
import { getParentSpec } from "./get-parent-spec"
import { hashSpec } from "./hash-spec"
import { loadPack } from "./load-pack"
import { savePack } from "./save-pack"
import { toBuffer } from "./to-buffer"
import { PackInfo, PackSpec, Sourcer, Transform } from "./types"

const { equal, ok: _ok } = strictAssert

function ok<T>(
  value: T,
  message?: string | Error
): asserts value is T extends null | undefined | false | 0 | "" ? never : T {
  return _ok(value, message)
}

export default fp<unknown, unknown, unknown, PipelinesPluginOptions>(
  async function pipelinesPlugin(server, { workerMode }) {
    const sourcersMap = new Map<string, SourcerRegistrationOptions>()
    const transformsMap = new Map<string, TransformRegistrationOptions>()

    const {
      mongo,
      rabbitmq,
      rabbitmq: { channel, publish, subscribe },
      minio
    } = server
    const db = mongo.client.db()

    const packsCollection = db.collection<PackInfo>("packs")

    server.decorate("pipelines", {
      registerSourcer(opts: SourcerRegistrationOptions) {
        const { type } = opts
        if (sourcersMap.has(type)) {
          throw new TypeError(
            `The sourcer type ${type} has already been registered.`
          )
        }
        sourcersMap.set(type, opts)
      },
      registerTransform(opts: TransformRegistrationOptions) {
        const { type } = opts
        if (transformsMap.has(type)) {
          throw new TypeError(
            `The transform type ${type} has already been registered.`
          )
        }
        transformsMap.set(type, opts)
      },
      async enqueue(spec: PackSpec) {
        await enqueue(packsCollection, rabbitmq, spec)
      }
    })

    server.post("/enqueue", async request => {
      const spec = request.body as PackSpec
      await enqueue(packsCollection, rabbitmq, spec)
      return { id: hashSpec(spec) }
    })

    async function getPackFile(hash: string, path: string) {
      const packInfo = await packsCollection.findOne({ _id: hash })
      if (packInfo == null) {
        throw new HttpErrors.NotFound("Pack not found.")
      }
      if (!packInfo.exists) {
        for await (const msg of subscribe("complete")) {
          if (msg.content.toString("utf8") !== hash) continue
        }
      }
      return minio.client.getObject(minio.bucketName, `${hash}/${path}`)
    }

    server.get("/get/:hash/*", async request => {
      const { hash, "*": path } = request.params as Record<"hash" | "*", string>
      return await getPackFile(hash, path)
    })

    // Process inputs and transforms
    if (workerMode) {
      const {
        sourcersFilter = [],
        transformsFilter = [],
        concurrency = 1
      } = workerMode

      channel.prefetch(concurrency)

      // Get processors running.
      // Note that only the provided input / transform types are ever processed.

      for (const type of sourcersFilter) {
        const subscription = subscribe(`input.${type}`)
        ;(async () => {
          for await (const msg of subscription) {
            await processMessage(msg, true, type)
          }
        })()
      }
      for (const type of transformsFilter) {
        const subscription = subscribe(`transform.${type}`)
        ;(async () => {
          for await (const msg of subscription) {
            await processMessage(msg, false, type)
          }
        })()
      }
    }

    async function processMessage(
      msg: ConsumeMessage,
      isSource: boolean,
      matchType: string
    ) {
      try {
        const spec = JSON.parse(msg.content.toString()) as PackSpec
        const hash = hashSpec({ source: spec.source, transforms: [] })
        const info = (await packsCollection.findOne({ _id: hash }))!

        if (isSource) {
          // Perform sourcing.
          equal(matchType, spec.source.type, "Input type did not match.")

          if (!info.exists) {
            const sourcer = sourcersMap.get(spec.source.type)
            ok(sourcer, `Sourcer ${spec.source.type} not found.`)

            const pack = await sourcer!.fn(spec.source)

            // Save pack to minio.
            await savePack(minio, spec, pack, true)

            // Save metadata.
            await packsCollection.updateOne(
              { _id: hash },
              { $set: { meta: pack.meta } }
            )
          }

          // Start next transform.
          publish("complete", toBuffer(hash))
        } else {
          // Perform matching transform.
          const transformSpec = spec.transforms.slice(0, -1)[0]
          equal(matchType, transformSpec.type, "Transform type did not match.")

          if (!info.exists) {
            const transform = transformsMap.get(transformSpec.type)
            ok(transform, `Transform ${transformSpec.type} not found.`)

            const inputPack = loadPack(minio, getParentSpec(spec)!, info.meta)

            const outputPack = await transform.fn(transformSpec, inputPack)

            await savePack(minio, spec, outputPack, true)
            await packsCollection.updateOne(
              { _id: hash },
              { $set: { meta: outputPack.meta } }
            )
          }

          publish("complete", toBuffer(hash))
        }

        channel.ack(msg)
      } catch (err) {
        channel.nack(msg)
      }
    }
  },
  {
    name: "@chaos-nexus/plugin-pipelines",
    fastify: "^2.0.0",
    dependencies: [
      "@chaos-nexus/decorator-minio",
      "@chaos-nexus/decorator-rabbitmq",
      "fastify-mongodb"
    ],
    decorators: { fastify: ["minio", "rabbitmq", "mongo"] }
  }
)

export interface PipelinesPluginOptions {
  workerMode?: {
    sourcersFilter?: string[]
    transformsFilter?: string[]
    concurrency?: number
  }
}

export interface SourcerRegistrationOptions {
  type: string
  schema: object
  fn: Sourcer
}

export interface TransformRegistrationOptions {
  type: string
  schema: object
  fn: Transform
}

declare module "fastify" {
  interface FastifyInstance<HttpServer, HttpRequest, HttpResponse> {
    pipelines: {
      registerSourcer(options: SourcerRegistrationOptions): void
      registerTransform(options: TransformRegistrationOptions): void
      enqueue(spec: PackSpec): Promise<void>
    }
  }
}
