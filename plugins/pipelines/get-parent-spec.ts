import { PackSpec } from "./types"

/**
 * Given a spec, returns its parent. Source-only specs get returned null.
 * Specs that have been transformed will be returned the spec before its
 * last transform.
 */
export function getParentSpec(spec: PackSpec): PackSpec | null {
  if (spec.transforms.length === 0) {
    return null
  }
  return { source: spec.source, transforms: spec.transforms.slice(0, -1) }
}
