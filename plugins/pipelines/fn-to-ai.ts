/**
 * Creates an async iterable from a function that returns an async iterator.
 */
export function fnToAi<T>(
  fn: () => AsyncIterator<T> | AsyncIterableIterator<T>
): AsyncIterable<T> {
  return {
    [Symbol.asyncIterator]() {
      const iterator = isAsyncIterable<T>(fn)
        ? fn[Symbol.asyncIterator]()
        : fn()
      return iterator
    }
  }
}

function isAsyncIterable<T>(x: any): x is AsyncIterable<T> {
  return typeof x[Symbol.asyncIterator] === "function"
}
