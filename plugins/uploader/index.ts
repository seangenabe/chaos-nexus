import "@chaos-nexus/decorator-minio"
import "@chaos-nexus/decorator-multipart-ai"
import "fastify"
import "fastify-multipart"
import fp from "fastify-plugin"
import S from "fluent-schema"
import { BadRequest } from "http-errors"
import { from } from "ix/asynciterable"
import { map } from "ix/asynciterable/operators/map"
import { toArray } from "ix/asynciterable/toarray"
import { UploadInputEntry } from "./types"
import { uploadFiles } from "./upload-files"

export default fp<unknown, unknown, unknown, UploaderOptions>(
  async function uploaderPlugin(server, { dir = "uploads" }) {
    server.decorate("uploader", { uploadFiles, dir })

    server.post(
      "/upload",
      {
        schema: {
          response: {
            200: S.array().items(
              S.object()
                .required(["name"])
                .prop("name", S.string())
            )
          }
        }
      },
      async request => {
        if (!request.isMultipart) {
          throw new BadRequest("Must be a multipart request.")
        }
        const entries = request.multipartAi()

        const uploadFilesInput = from(entries).pipe(
          map(
            ({ field, file, filename }) =>
              ({
                file,
                name: `${field}/${filename}`
              } as UploadInputEntry)
          )
        )

        const uploadedFiles = uploadFiles(server, dir, uploadFilesInput)

        return await toArray(uploadedFiles)
      }
    )
  },
  {
    name: "@chaos-neuxs/plugin-uploader",
    fastify: "^2.0.0",
    dependencies: [
      "@chaos-nexus/decorator-minio",
      "@chaos-nexus/decorator-multipart-ai"
    ],
    decorators: { fastify: ["minio", "multipartAi"] }
  }
)

export interface UploaderOptions {
  dir?: string
}

export interface UploaderDecorator {
  uploadFiles: typeof uploadFiles
  dir: string
}

declare module "fastify" {
  interface FastifyInstance {
    uploader: UploaderDecorator
  }
}
