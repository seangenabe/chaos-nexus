import { Readable } from "stream"

export interface UploadInputEntry {
  file: Readable | Buffer | string
  name?: string
}

export interface UploadedFile {
  id: string
  name?: string
}
