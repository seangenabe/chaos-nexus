import "@chaos-nexus/decorator-minio"
import { map as asyncIterableMap } from "async-iterable-map"
import cuid from "cuid"
import { FastifyInstance } from "fastify"
import { UploadedFile, UploadInputEntry } from "./types"

export function uploadFiles<HttpServer, HttpRequest, HttpResponse>(
  server: FastifyInstance<HttpServer, HttpRequest, HttpResponse>,
  dir: string,
  entries: AsyncIterable<UploadInputEntry>
) {
  const { client, bucketName } = server.minio
  return asyncIterableMap<UploadInputEntry, UploadedFile>(
    entries,
    async ({ file, name }) => {
      const id = cuid()
      await client.putObject(bucketName, `${dir}/${id}`, file)
      return { id, name }
    },
    { concurrency: 4 }
  )
}
