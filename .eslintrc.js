module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: ["./packages/*/tsconfig.json"]
  },
  plugins: ["@typescript-eslint", "unicorn"],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking"
  ],
  rules: {
    "@typescript-eslint/member-delimiter-style": [
      "error",
      {
        multiline: { delimiter: "none" },
        singleline: { delimiter: "semi" }
      }
    ],
    "@typescript-eslint/array-type": [
      "error",
      { default: "array-simple", readonly: "array-simple" }
    ],
    "@typescript-eslint/consistent-type-definitions": ["error", "interface"],
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/func-call-spacing": "error",
    "@typescript-eslint/no-dynamic-delete": "error",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/no-extra-non-null-assertion": "error",
    "@typescript-eslint/no-extra-semi": "error",
    "@typescript-eslint/no-extra-semi": "error",
    "@typescript-eslint/no-extraneous-class": "error",
    "@typescript-eslint/no-misused-promises": "off",
    "@typescript-eslint/no-non-null-assertion": "off",
    "@typescript-eslint/no-magic-numbers": [
      "error",
      {
        ignore: [-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 30, 100, 1000],
        ignoreNumericLiteralTypes: true,
        ignoreReadonlyClassProperties: true,
        ignoreEnums: true
      }
    ],
    "@typescript-eslint/no-use-before-define": [
      "error",
      { functions: false, classes: false }
    ],
    "@typescript-eslint/prefer-for-of": "error",
    "@typescript-eslint/prefer-nullish-coalescing": "error",
    "@typescript-eslint/prefer-optional-chain": "error",
    "@typescript-eslint/prefer-readonly": "error",
    "@typescript-eslint/quotes": "error",
    "@typescript-eslint/require-await": "off",
    "@typescript-eslint/restrict-plus-operands": "error",
    "@typescript-eslint/semi": ["error", "never"],
    "@typescript-eslint/space-before-function-paren": [
      "error",
      {
        anonymous: "never",
        named: "never",
        asyncArrow: "always"
      }
    ],
    "class-methods-use-this": "error",
    "consistent-return": "error",
    "func-call-spacing": "off",
    "max-len": ["error", { code: 80, ignoreComments: true }],
    "no-constant-condition": "off",
    "no-dupe-else-if": "error",
    "no-extra-semi": "off",
    "no-magic-numbers": "off",
    "no-var": "error",
    quotes: "off",
    "require-atomic-updates": "error",
    semi: "off",
    "space-before-function-paren": "off",
    "space-in-parens": ["error", "never"],
    "unicorn/catch-error-name": [
      "warn",
      { caughtErrorsIgnorePattern: "(?:E|e)rr$" }
    ],
    "unicorn/escape-case": "error",
    "unicorn/error-message": "warn",
    "unicorn/explicit-length-check": ["error", { "non-zero": "not-equal" }],
    "unicorn/filename-case": ["error", { case: "kebabCase" }],
    "unicorn/import-index": "error",
    "unicorn/new-for-builtins": "error",
    "unicorn/no-array-instanceof": "error",
    "unicorn/no-console-spaces": "error",
    "unicorn/no-fn-reference-in-iterator": "error",
    "unicorn/no-keyword-prefix": "error",
    "unicorn/no-nested-ternary": "error",
    "unicorn/no-new-buffer": "error",
    "unicorn/no-process-exit": "error",
    "unicorn/no-unsafe-regex": "error",
    "unicorn/no-zero-fractions": "error",
    "unicorn/number-literal-case": "error",
    "unicorn/prefer-exponentiation-operator": "error",
    "unicorn/prefer-flat-map": "error",
    "unicorn/prefer-includes": "error",
    "unicorn/prefer-negative-index": "error",
    "unicorn/prefer-spread": "error",
    "unicorn/prefer-starts-ends-with": "error",
    "unicorn/prefer-string-slice": "error",
    "unicorn/prefer-text-content": "error",
    "unicorn/prefer-type-error": "error",
    "unicorn/regex-shorthand": "error",
    "unicorn/throw-new-error": "error"
  }
}
