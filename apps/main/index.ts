import media from "@chaos-nexus/plugin-media"
import Fastify from "fastify"
import { IncomingMessage, Server, ServerResponse } from "http"
import { Http2Server, Http2ServerRequest, Http2ServerResponse } from "http2"
import SwaggerPlugin from "fastify-swagger"
import MongodbPlugin from "fastify-mongodb"

export function createAllServer<
  HttpServer extends Server | Http2Server,
  HttpRequest extends Http2ServerRequest | IncomingMessage,
  HttpResponse extends Http2ServerResponse | ServerResponse
>() {
  const server = Fastify<HttpServer, HttpRequest, HttpResponse>()

  server.register(SwaggerPlugin, {
    swagger: {
      info: {
        title: "chaos-nexus",
        description: "",
        version: "1.0.0"
      },
      consumes: ["application/json"],
      produces: ["application/json"]
    },
    exposeRoute: true
  })
  server.register(MongodbPlugin as any, {
    url: "mongodb://localhost/chaos-nexus"
  })
  server.register(media)

  return server
}

if (require.main === module) {
  createAllServer().listen({ port: 3000 }, (err, address) => {
    console.log(err)
    console.log(address)
  })
}
